import { FETCH_TODOS, ADD_TODO, UPDATE_TODO, DELETE_TODO } from './actionTypes'

export function fetchTodos() {
  return {
    type: FETCH_TODOS,
    payload: {
      request: {
        url: '/todos.json',
      },
    },
  }
}

export function addTodo(todo) {
  return {
    type: ADD_TODO,
    payload: {
      request: {
        url: '/todos.json',
        method: 'POST',
        data: todo,
      },
    },
  }
}

export function updateTodo({ id, ...todo }) {
  return {
    type: UPDATE_TODO,
    payload: {
      request: {
        url: `/todos/${id}.json`,
        method: 'PUT',
        data: todo,
      },
    },
  }
}

export function deleteTodo(id) {
  return {
    type: DELETE_TODO,
    payload: {
      request: {
        url: `/todos/${id}.json`,
        method: 'DELETE',
      },
    },
  }
}
