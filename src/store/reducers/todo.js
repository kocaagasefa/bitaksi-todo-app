import {
  actionSuccess,
  ADD_TODO,
  DELETE_TODO,
  FETCH_TODOS,
  UPDATE_TODO,
} from '../actions/actionTypes'

const initialState = []

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionSuccess(FETCH_TODOS):
      return convertTodosToArray(action.payload.data || {})

    case actionSuccess(ADD_TODO):
      const newTodo = action.meta.previousAction.payload.request.data
      const newTodoId = action.payload.data.name

      return [
        ...state,
        {
          ...newTodo,
          id: newTodoId,
        },
      ]

    case actionSuccess(UPDATE_TODO):
      const updatedTodoId = action.payload.config.url.match(
        '/todos/(.*).json'
      )[1]

      return state.map((todo) =>
        todo.id === updatedTodoId
          ? { ...action.payload.data, id: updatedTodoId }
          : todo
      )

    case actionSuccess(DELETE_TODO):
      const deletedTodoId = action.payload.config.url.match(
        '/todos/(.*).json'
      )[1]

      return state.filter((todo) => todo.id !== deletedTodoId)

    default:
      return state
  }
}
export default todoReducer

function convertTodosToArray(todosObject = {}) {
  return Object.keys(todosObject).map((todoId) => ({
    id: todoId,
    ...todosObject[todoId],
  }))
}
