import axios from 'axios'

const baseURL = process.env.REACT_APP_API_URL

export const middlewareConfig = {
  interceptors: {
    request: [
      {
        success: function ({ getState, dispatch, getSourceAction }, req) {
          return req
        },
        error: function ({ getState, dispatch, getSourceAction }, error) {
          return error
        },
      },
    ],
    response: [
      {
        success: function ({ getState, dispatch, getSourceAction }, res) {
          return Promise.resolve(res)
        },
        error: function ({ getState, dispatch, getSourceAction }, error) {
          return Promise.reject(error)
        },
      },
    ],
  },
}

const client = axios.create({
  baseURL,
  responseType: 'json',
})

export default client
