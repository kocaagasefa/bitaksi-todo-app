import React, { useState } from 'react'
import { Form, Row, Col, Button, Input } from 'antd'
import { PlusCircleFilled } from '@ant-design/icons'
import { useDispatch } from 'react-redux'
import { addTodo } from '../../store/actions/todoActions'
import classes from './style.module.css'

const AddTodo = () => {
  const [form] = Form.useForm()
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const onFinish = () => {
    setLoading(true)
    dispatch(
      addTodo({
        name: form.getFieldValue('name'),
        completed: false,
      })
    ).then(() => setLoading(false))

    form.resetFields()
  }

  return (
    <Form
      form={form}
      onFinish={onFinish}
      layout="horizontal"
      className={classes.addTodo}
    >
      <Row gutter={20}>
        <Col xs={24} md={17} lg={20}>
          <Form.Item
            name={'name'}
            rules={[{ required: true, message: 'This field is required' }]}
          >
            <Input placeholder="New todo..." disabled={loading} />
          </Form.Item>
        </Col>
        <Col xs={24} md={7} lg={4}>
          <Button type="primary" htmlType="submit" block loading={loading}>
            <PlusCircleFilled />
            Add
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

export default AddTodo
