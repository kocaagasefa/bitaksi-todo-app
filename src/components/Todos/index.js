import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, Card, List, message, Empty } from 'antd'
import { fetchTodos } from '../../store/actions/todoActions'
import AddTodo from '../AddTodo'
import classes from './style.module.css'
import TodoItem from '../TodoItem'

const Todos = () => {
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const todos = useSelector((state) => state.todos)

  useEffect(() => {
    setLoading(true)

    dispatch(fetchTodos()).then((res) => {
      setLoading(false)
      if (res.error) message.error(res.error.data || 'Network Error!')
    })
  }, [dispatch])

  return (
    <Row
      justify="center"
      align="middle"
      gutter={[0, 10]}
      className={classes.todos}
    >
      <Col xs={{ span: 23 }} md={{ span: 21 }} xl={{ span: 18 }}>
        <Card title="Add New Todo" size="small">
          <AddTodo />
        </Card>
      </Col>

      <Col xs={{ span: 23 }} md={{ span: 21 }} xl={{ span: 18 }}>
        <Card title="Todo List" size="small">
          <List loading={loading} size="small">
            {todos.map((todo) => (
              <TodoItem key={todo.id} todo={todo} />
            ))}
          </List>

          {!todos[0] && <Empty />}
        </Card>
      </Col>
    </Row>
  )
}

export default Todos
