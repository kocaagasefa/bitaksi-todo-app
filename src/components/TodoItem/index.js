import React, { useState } from 'react'
import { Tooltip, Tag, List, Button, Popconfirm, message } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import { useDispatch } from 'react-redux'
import { updateTodo, deleteTodo } from '../../store/actions/todoActions'
import Text from 'antd/lib/typography/Text'
import classes from './style.module.css'

const TodoItem = ({ todo }) => {
  const dispatch = useDispatch()
  const [toggleLoading, setToggleLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)

  const toggleTodo = () => {
    setToggleLoading(true)

    dispatch(
      updateTodo({
        ...todo,
        completed: !todo.completed,
      })
    ).then((res) => {
      setToggleLoading(false)
      if (res.error) message.error('Todo Update Error!')
    })
  }

  const removeTodo = () => {
    setDeleteLoading(true)
    dispatch(deleteTodo(todo.id)).then((res) => {
      if (res.error) {
        setDeleteLoading(false)
        message.error('Todo Delete Error!')
      }
    })
  }

  return (
    <List.Item
      actions={[
        <Tooltip
          title={todo.completed ? 'Mark as uncompleted' : 'Mark as completed'}
        >
          <Button
            type={todo.completed ? 'primary' : null}
            onClick={toggleTodo}
            shape="circle"
            loading={toggleLoading}
            icon={<CheckOutlined />}
          />
        </Tooltip>,
        <Popconfirm
          title="Are you sure you want to delete?"
          onConfirm={removeTodo}
        >
          <Button
            type="primary"
            danger
            shape="circle"
            loading={deleteLoading}
            icon={<CloseOutlined />}
          />
        </Popconfirm>,
      ]}
      key={todo.id}
    >
      <div className={classes.todoItem}>
        <Tag
          color={todo.completed ? 'cyan' : 'red'}
          className={classes.todoText}
        >
          <Text delete={todo.completed}>{todo.name}</Text>
        </Tag>
      </div>
    </List.Item>
  )
}

export default TodoItem
